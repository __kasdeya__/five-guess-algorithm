use rand::Rng;
use std::fmt;

#[derive(Debug)]
pub struct Pattern(pub Vec<char>);

impl Pattern {
    pub fn make_random(pegs: u32, colors: u32) -> Self {
        let mut rng = rand::thread_rng();
        let mut pattern = Vec::new();

        for _ in 0..pegs {
            let next_peg = rng.gen_range(0..colors);
            let next_peg = char::from_digit(next_peg, colors)
                .expect("couldn't convert digit to char!");
            pattern.push(next_peg);
        }

        Self(pattern)
    }

    pub fn len(&self) -> usize { self.0.len() }
}

impl fmt::Display for Pattern {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Write strictly the first element into the supplied output
        // stream: `f`. Returns `fmt::Result` which indicates whether the
        // operation succeeded or failed. Note that `write!` uses syntax which
        // is very similar to `println!`.
        let pattern_str: String = self.0.iter().collect();
        write!(f, "{pattern_str}")
    }
}
