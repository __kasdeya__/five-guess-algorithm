mod pattern;
use pattern::Pattern;

mod judge;
use judge::judge;

fn main() {
    let secret = Pattern(vec!['1', '1', '2', '2']);

    for _ in 0..100 {
        let guess = Pattern::make_random(4, 6);
        let judgement = judge(secret, guess)
            .expect("something broke");
        println!("pattern: {guess}  b:{} w:{}", judgement.black, judgement.white);
    }
}
