#[path = "pattern.rs"] mod pattern;
use pattern::Pattern;

pub struct Judgement {
    pub black: u32,
    pub white: u32,
}

pub fn judge(secret: Pattern, guess: Pattern) -> Result<Judgement, String> {
    dbg!(secret);
    dbg!(guess);
    if secret.len() != guess.len() {
        return Err("secret and guess lengths differ".to_string());
    }

    // which indicies have we already found pegs for?
    let mut pegged = vec![false; secret.len()];
    dbg!(pegged);

    Ok(Judgement {black: 0, white: 0})
}
